(ns youkan.views
  (:require [hiccup.page :refer [html5 include-css include-js]]))

(def index (html5
             [:head
              [:meta
               {:charset "utf-8"}]
              [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
              [:title "index"]
              (include-css "/css/site.css")]
             [:body
              [:div {:id "app"}]
              (include-js "/js/app.js")
              (include-js "https://use.fontawesome.com/releases/v5.7.1/js/all.js")
              [:script "youkan_front.core.init();"]]))

