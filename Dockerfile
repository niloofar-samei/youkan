FROM java:8-alpine
MAINTAINER Your Name <you@example.com>

ADD target/youkan-0.0.1-SNAPSHOT-standalone.jar /youkan/app.jar

EXPOSE 8080

CMD ["java", "-jar", "/youkan/app.jar"]
