(ns youkan-front.events
  (:require
   [re-frame.core :as rf]
   [youkan-front.db :as db]
   [youkan-front.input-validation :as iv]
   [youkan-front.graphql :as gql]
   [day8.re-frame.tracing :refer-macros [fn-traced defn-traced]]))

(defn assoc-multi [db data]
  " Assoc multiple data into database, return resulting database.
  (reduce assoc-multi [db
                        [[:key :subkey] \"data\"]
                        [[:key :subkey2] \"data2\"]])"
  (assoc-in db (first data) (second data)))

(rf/reg-event-db
 ::initialize-db
 (fn-traced [_ _]
   db/default-db))

(rf/reg-event-fx
  ; Show signup modal.
  :show-signup
  (fn [{:keys [db]} [_ _]]
    {:db (assoc db :modal {:signup "is-active"})}))

(rf/reg-event-fx
  ; Show login modal.
  :show-login
  (fn [{:keys [db]} [_ _]]
    {:db (assoc db :modal {:login "is-active"})}))

(rf/reg-event-fx
  ; Hide all modals.
  :hide-modals
  (fn [{:keys [db]} [_ _]]
    {:db (dissoc db :modal)}))

(rf/reg-event-fx
  ; Toggle burger menu.
  :burger
  (fn [{:keys [db]} [_ _]]
    (let [state (get-in db [:burger])]
      (if (= state "is-active")
        {:db (assoc db :burger "")}
        {:db (assoc db :burger "is-active")}))))

(rf/reg-event-fx
  ; Validate email input and only if it fits requirements, add it to db.
  :login-email
  (fn [{:keys [db]} [_ email]]
    (if (empty? email)
      {:db (reduce assoc-multi [db
                                [[:user :email] ""]
                                [[:login-help :email] ""]])}
      (if (iv/email email)
        {:db (reduce assoc-multi [db
                                  [[:user :email] email]
                                  [[:login-help :email] "is-primary"]])}
        {:db (reduce assoc-multi [db
                                  [[:user :email] ""]
                                  [[:login-help :email] "is-danger"]])}))))

(rf/reg-event-fx
  ; Validate password input and only if it fits requirements, add it to db.
  :login-password
  (fn [{:keys [db]} [_ password]]
    (if (empty? password)
      {:db (reduce assoc-multi [db
                                [[:user :password] ""]
                                [[:login-help :password] ""]])}
      (if (iv/password password)
        {:db (reduce assoc-multi [db
                                  [[:user :password] password]
                                  [[:login-help :password] "is-primary"]])}
        {:db (reduce assoc-multi [db
                                  [[:user :password] ""]
                                  [[:login-help :password] "is-danger"]])}))))

(rf/reg-event-fx
  ; Check if username is valid. If so, add it to db.
  :signup-username
  (fn [{:keys [db]} [_ username]]
    (if (empty? username)
      {:db (reduce assoc-multi [db
                                [[:user :username] ""]
                                [[:signup-help :username] ""]])}
      (if (iv/username username)
        {:db (reduce assoc-multi [db
                                  [[:user :username] username]
                                  [[:signup-help :username] "is-primary"]])}
        {:db (reduce assoc-multi [db
                                  [[:user :username] ""]
                                  [[:signup-help :username] "is-danger"]])}))))

(rf/reg-event-fx
  ; Validate email input and only if it fits requirements, add it to db.
  :signup-email
  (fn [{:keys [db]} [_ email]]
    (if (empty? email)
      {:db (reduce assoc-multi [db
                                [[:user :email] ""]
                                [[:signup-help :email] ""]])}
      (if (iv/email email)
        {:db (reduce assoc-multi [db
                                  [[:user :email] email]
                                  [[:signup-help :email] "is-primary"]])}
        {:db (reduce assoc-multi [db
                                  [[:user :email] ""]
                                  [[:signup-help :email] "is-danger"]])}))))

(rf/reg-event-fx
  ; Validate password input and only if it fits requirements, add it to db.
  ; Check if `password` and `repeat` are the same. If so, change `repeat` style to `is-primary`. 
  :signup-password
  (fn [{:keys [db]} [_ password]]
    (if (empty? password)
      {:db (reduce assoc-multi [db
                                [[:user :password] ""]
                                [[:signup-help :password] ""]])}
      (let [valid (iv/password password)
            confirmed (= (get-in db [:user :repeat]) password)]
        (if valid
          (if confirmed
            {:db (reduce assoc-multi [db
                                      [[:user :password] password]
                                      [[:signup-help :password] "is-primary"]
                                      [[:signup-help :repeat] "is-primary"]])}
            {:db (reduce assoc-multi [db
                                      [[:user :password] password]
                                      [[:signup-help :password] "is-primary"]
                                      [[:signup-help :repeat] "is-danger"]])})
          {:db (reduce assoc-multi [db
                                    [[:user :password] ""]
                                    [[:signup-help :password] "is-danger"]
                                    [[:signup-help :repeat] ""]])})))))

(rf/reg-event-fx
  ; Check if `password` and `repeat` are the same. If so, change `repeat` style to `is-primary`. 
  :signup-repeat
  (fn [{:keys [db]} [_ rp]]
    (if (empty? rp)
      {:db (reduce assoc-multi [db
                                [[:user :repeat] ""]
                                [[:signup-help :repeat] ""]])}
      (if (= (get-in db [:user :password]) rp)
        {:db (reduce assoc-multi [db
                                  [[:user :repeat] rp]
                                  [[:signup-help :repeat] "is-primary"]])}
        {:db (reduce assoc-multi [db
                                  [[:user :repeat] rp]
                                  [[:signup-help :repeat] "is-danger"]])}))))

(rf/reg-event-fx
  ;submit login form
  :submit-login
  (fn [{:keys [db]} [_ _]]
    (let [email (get-in db [:user :email])
          password (get-in db [:user :password])]
      (if (and email
               password
               (not= email "")
               (not= password ""))
        (gql/login email password)
        {:db (assoc-in db [:login-help :submit] "is-danger")}))))

(rf/reg-event-fx
  ;submit signup form
  :submit-signup
  (fn [{:keys [db]} [_ _]]
    (let [username (get-in db [:user :username])
          email (get-in db [:user :email])
          password (get-in db [:user :password])
          rp (get-in db [:user :repeat])]
      (if (and username
               email
               password
               rp
               (not= username "")
               (not= email "")
               (not= password "")
               (not= rp ""))
        (gql/signup username email password)
        {:db (assoc-in db [:signup-help :submit] "is-danger")}))))

